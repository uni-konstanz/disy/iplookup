/*
Copyright (c) 2009 thomas.zink _at_ uni-konstanz _dot_ de

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include <math.h>
#include <list>
#include <set>
#include <vector>


#define LOGX(x,b)       ( log((x))/log((b)) )
#define MRAND(x)        ( rand()%(x) )
#define RRAND(l,u)	    ( rand()%(u)+(l) )
#define M(n)            ( (int)pow(2,ceil(LOGX(12.8*(n),2))) )
#define N(m)            ( (int)pow(2,floor(LOGX((m),2)-LOGX(12.8,2))) )

#define SUCCESS 0
#define FAILURE 1
#define DEBUG 0

using namespace std;

/* TYPEDEFS
******************************************************************************/

typedef uint32_t k_t;
typedef uint16_t v_t;

/* GLOBALS
******************************************************************************/

static const unsigned long POLY[] = {
	0xEDB88320, 0x82F63B78, 0xEB31D82E
};

/* TRANSFORMATION AND HASH FUNCTIONS
******************************************************************************/

typedef k_t (*transfn_t) (k_t, ...);
typedef uint32_t (*hashfn_t) (k_t, ...);

k_t identity (k_t k) { return k; }

k_t crc32 (k_t k, uint32_t *crct)
{
	uint32_t xmask = -1, crc;
	crc = xmask;
	uint8_t len = sizeof(k);
	uint32_t *pk = &k;
	uint8_t *p = (uint8_t*)pk;
	for (uint8_t i=0; i<len; i++) {
		crc = crct[(crc^p[i]) & 0xFF] ^ (crc>>8);
	}
	return crc ^ xmask;
}

uint32_t uhash(k_t k, uint8_t bits, uint32_t *lut)
{
    uint32_t h=0;
    for (uint8_t b=0; b<bits; b++, lut++) h ^= (((k >> b) & 1) == 1) ? *lut : 0;
    return h;
}

uint32_t bitx(k_t k, size_t start, size_t stop)
{
	uint32_t h = 0, mask = (uint32_t)(pow(2,stop-start+1)-1);
	h = (k >> start) & mask;
	return h;
}

/* LOOKUP TABLE ALLOCATORS
******************************************************************************/

uint32_t * rndtbl(size_t len, uint32_t max)
{
	uint32_t * lut = new uint32_t[len];
	for (size_t i = 0; i < len; i++) lut[i] = RRAND(1,max);
	return lut;
}

uint32_t * crctbl(uint8_t num)
{
	uint32_t * lut = new uint32_t[256*num*sizeof(uint32_t)];
	uint32_t * p = lut;
	for (uint8_t i = 0; i < num; i++) {
		uint16_t c;
		for (uint16_t n = 0; n <= 255; n++, p++) {
			c = n;
			for (uint8_t k = 0; k < 8; k++) {
				if (c & 1) c = POLY[i] ^ (c >> 1);
				else c = c >> 1;
			}
		}
		*p = c;
	}
	return lut;
}

/* CLASS ENTRY
******************************************************************************/

class entry_t
{
public:
    k_t _k;
    v_t _v;
    entry_t (k_t k=0, v_t v=0) { _k = k; _v = v; }
    void print() { printf("(%d,%d)",_k,_v); }
};

/* CLASS EHT
******************************************************************************/

class eht_t
{
private:
	/* MEMBERS ********************************/
	uint32_t _n;         // max number of items
    uint32_t _m;         // number of cells / buckets
	uint8_t _mspace;     // address space of _m in number of bits
	uint8_t _k;          // number of hash functions to use
	uint8_t _w;			 // number of buckets per cell
    uint8_t _b;          // number of bits to check
	uint32_t _fp;		 // false positive counter
    uint32_t * _lut;     // lookup table for random numbers
    entry_t * _buck;     // hashtable buckets
    uint32_t _bucksize;   // number of occupied buckets
    uint8_t * _cell;     // hashfilter cells
    uint32_t _cellsize;   // number of occupied cells
    list<entry_t> _cam;  // cam list
    uint32_t _critical;  // counter for critical errors
    
	/* TRANSFORM, FINGERPRINT, HASH ************/
	inline vector<k_t> transform(k_t k)
	{
		vector<k_t> keys;
		for (uint8_t i = 0; i < _k; i++) keys.push_back(crc32(k,_lut+(i*256)));
		return keys;
	}
	
	inline vector<uint32_t> fingerprint(k_t k)
	{
		vector<uint32_t> f;
		for (uint8_t i = 0; i < _k; i++) f.push_back(bitx(k, _mspace-1,32));
		return f;
	}
	
    inline set<uint32_t> _uhash(k_t k)
    {
        set<uint32_t> h;
        for (uint8_t i = 0; i < _k; i++) h.insert(uhash(k,_b,_lut+(_b*i)));
        return h;
    }
	
	inline vector<uint32_t> _bithash(k_t k)
	{
		vector<uint32_t> h;
		for (uint8_t i = 0; i< _k; i++) h.push_back(bitx(k, 0, _mspace-1));
		return h;
	}
    
public:
    eht_t (uint32_t n, uint8_t mshift, uint8_t w)
    {
        _n = n;
        _m = M(_n) >> mshift;
		_mspace = LOGX(_m,2);
		_w = w;
        _k = 3;
		_b = 32;
		_fp = 0;
		_lut = rndtbl(_k*_b, _m-1);
        _cell = new uint8_t [_m];
        _cellsize = 0;
        for (uint32_t i = 0; i < _m ; i++) _cell[i] = _k;
        _buck = new entry_t [_m*_w];
        _bucksize = 0;
        _critical = 0;
    }
    
    ~eht_t () { delete [] _cell; delete [] _buck; }

    int put (k_t k, v_t v)
    {
        // hash
        set<uint32_t> hashs = _uhash(k);
		// find counter
        uint8_t i = 0;
        for (set<uint32_t>::iterator it = hashs.begin(); it != hashs.end(); it++, i++) {
            uint32_t h = *it;
            // if counter == _k, it's not occupied
            if (_cell[h] == _k) {
                _cell[h] = i;
                _cellsize++;
                _buck[h*_w] = entry_t (k,v);
                _bucksize++;
                return SUCCESS;
            }
            // if counter == i, it's most probably this key
            if (_cell[h] == i) {
				// first check if key already present
				for (uint8_t w = 0; w < _w; w++) {
					if (_buck[h*_w+w]._k == k) {
						_buck[h*_w+w]._v = v;
						return SUCCESS;
					}
				}
				// if not put into first empty bucket
				for (uint8_t w = 0; w < _w; w++) {
					if (_buck[h*_w+w]._k == 0) {
						_buck[h*_w+w]._k = k;
						_buck[h*_w+w]._v = v;
						_bucksize++;
						return SUCCESS;
					}
                    // if we reach this condition, all buckets full
                    if (w == _w-1) {
                        _cam.push_front(entry_t(k,v));
                        return SUCCESS;
                    }
				}
            }
            // on all other cases, check next hash value
        }
        // if we reach this point, we got a serious problem ...
        _critical++;
        return FAILURE;
    }

    v_t get (k_t k)
    {
        // hash
        set<uint32_t> hashs = _uhash(k);
        // find counter
        uint8_t fail = 0;
        uint8_t i = 0;
        for (set<uint32_t>::iterator it = hashs.begin(); it != hashs.end(); it++, i++) {
            uint32_t h = *it;
            // if counter == i, found key position
            if (_cell[h] == i) {
				for (uint8_t w = 0; w < _w; w++) {
					if (_buck[h*_w+w]._k == k) return _buck[h*_w+w]._v;
				}
				// no luck? need to turn to cam
                for (list<entry_t>::iterator it = _cam.begin(); it != _cam.end(); it++)
                    if ((*it)._k == k) return (*it)._v;
            }
            // if counter == _k, we fail here
            if (_cell[h] == _k) fail++;
        }
        // if fail == hashs.size, key cannot be present, has never been inserted
        if (fail == hashs.size()) return 0;
		// if we reach this point we got a fp;
		_fp++;
        return 0;
    }
    
    int rm (k_t k)
    {
        // hash
        set<uint32_t> hashs = _uhash(k);
        // find counter
        uint8_t i = 0, fail = 0;
        for (set<uint32_t>::iterator it = hashs.begin(); it != hashs.end(); it++, i++) {
            uint8_t h = *it;
            // if counter == i, found key position
            if (_cell[h] == i) {
				bool empty = false, deleted = false;
				for (uint8_t w = 0; w < _w; w++) {
					if (_buck[h*_w+w]._k == k) {
						_buck[h*_w+w]._k = 0;
						_buck[h*_w+w]._v = 0;
						_bucksize--;
						deleted = true;
					}
				}
				for (uint8_t w = 0; w < _w; w++) {
					if (_buck[h*_w+w]._k == 0) empty = true;
					else {
						empty = false;
						break;
					}
				}
				if (empty) {
					_cell[h] = _k;
					_cellsize--;
				}
				if (deleted) return SUCCESS;
				else {
                    for (list<entry_t>::iterator it = _cam.begin(); it != _cam.end(); it++)
                        if ((*it)._k == k) {
                            _cam.erase(it);
                            return SUCCESS;
                        }
                }
            }
            if (_cell[h] == _k) fail++;
        }
        if (fail == hashs.size()) return FAILURE;
        // if we reach this point we could not delete the item
        return FAILURE;
    }
    
    void printall()
	{
		// print general info
		printf("n,m,k,b,cells,bucks,cam,fp,critical\n%d,%d,%d,%d,%d,%d,%d,%d,%d\n",_n,_m,_k,_b,_cellsize,_bucksize,(int)_cam.size(),_fp,_critical);
		// print bloom filter
		for (uint32_t m = 0; m < _m; m++)
			printf("[%d](%d) ", m, (int)_cell[m]);
		cout << endl;
		// print hashtable
		for (uint32_t m = 0; m < _m; m++) {
			printf("[%d] ",m);
			for (uint8_t w=0; w < _w; w++) {
				printf("(%d,%d) ", _buck[m*_w+w]._k, _buck[m*_w+w]._v);
			}
			cout << endl;
		}
		// print cam
		for (list<entry_t>::iterator it = _cam.begin(); it != _cam.end(); it++) {
			printf("(%d,%d)->",(*it)._k, (*it)._v);
		}
		cout << endl;
	}
	
	void print()
    {
		printf("n,m,k,b,cells,bucks,cam,fp,critical\n%d,%d,%d,%d,%d,%d,%d,%d,%d\n",_n,_m,_k,_b,_cellsize,_bucksize,(int)_cam.size(),_fp,_critical);
    }
};


/* MAIN STUFF
******************************************************************************/

int batchmode(void);
int usage(void);

// STANDARD VALUES
uint32_t N = 10000;
uint8_t M = 0, S = 1, W = 2;
uint8_t MAXD = 31, QFACTOR = 2;

// MAIN
int main (int argc, char * const argv[]) {
    clock_t start = clock();
    extern char* optarg;
    char c;
    //if (argc<=1) return usage();
    // check arguments
    while ((c = getopt(argc, argv, "hn:m:w:s:")) != -1) {
        switch(c) {
        case 'h':
            return usage();
            break;
        case 'n':
            N = atoi(optarg);
            break;
        case 'm':
            M = atoi(optarg);
            break;
		case 'w':
			W = atoi(optarg);
			break;
        case 's':
            S = atoi(optarg);
            break;
        default:
            return usage();
        }
    }
    batchmode();
    clock_t end = clock();
    fprintf (stderr,"**************************************************\n");
    fprintf (stderr,"cycles: %d, seconds: %.3f\n",(int)(end-start),(float)(end-start)/CLOCKS_PER_SEC);
    return 0;
}

int usage(void)
{
    printf(
        "----------------------------------------\n"
        "|    Huawei - Konstanz - IP Lookup     |\n"
        "|      with Efficient Hash Tables      |\n"
        "----------------------------------------\n"
    );
    printf("usage: iplookup [-n #items] [-m #shifts] [-w width] [-s #simulations]\n");
    printf("\n-n #items\n");
    printf(
        "\tDenotes number of items to insert into eht.\n"
        "\tThe eht is created with optimal parameters for this number of items. No rehashing.\n"
        "\tDefault: 100.\n"
    );
    printf("\n-m #shifts\n");
    printf(
        "\tThe default address space for eht is M(n)= pow(2,ceil(log2(12.8*n))).\n"
        "\tM is shifted a number of bits equal to #shifts to reduce space requirements.\n"
        "\tDefault: 0.\n"
    );
    printf("\n-s #simulations\n");
    printf(
        "\tSets the number of simulations.\n"
        "\tEach iteration creates a new eht with passed arguments, fills it with a number\n"
        "\tof random items equal to n and queries for n*10 random items.\n"
        "\tOutput is generated on stdout on each run as well as a summary.\n"
        "\tOutput shows configuration parameters followed by the number of\n"
        "\tentries in offline, online and cam table followed by the false positive rate.\n"
        "\tIn addition The summary shows the average distribution of items in all data structures.\n"
        "\tDefault: 1.\n"
    );
	printf("\n-w bucket width\n");
	printf(
		"\tNumber of items per hashbucket.\n"
		"\tDefault: 2\n"
	);
    printf("\n-h\n");
    printf("\tPrints this help.\n");
    return 0;
}

int batchmode(void)
{
    for (uint8_t s = 0; s < S; s++) {
        // create new eht
        eht_t *p = new eht_t(N,M,W);
		vector<k_t> keys;
        // create new keys/vals
        for (uint32_t n = 0; n < 3*N; n++) {
            k_t k = RRAND(1,(int)pow(2,32));
            v_t v = RRAND(1,(int)pow(2,10));
            p->put(k,v);
			keys.push_back(k);
        }
        uint32_t got = 0;
		for (uint32_t i = 0; i < keys.size(); i++) {
			v_t v = p->get(keys[i]);
			if (v) got ++;
			else printf("error retrieving: %d\n", keys[i]);
		}
		for (uint32_t i = 0; i < 3*N*QFACTOR; i++) {
			k_t k = RRAND(1,(int)pow(2,32));
			p->get(k);
		}
		fprintf(stderr, "retrieved: %d / %d\n", got, (int)keys.size());
		p->printall();
		p->print();
        // delete
        delete p;
    }
	return 0;
}
