	Copyright (c) 2009 thomas.zink _at_ uni-konstanz _dot_ de

	Permission is hereby granted, free of charge, to any person obtaining a copy of
	this software and associated documentation files (the "Software"), to deal in 
	the Software without restriction, including without limitation the rights to use, 
	copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
	Software, and to permit persons to whom the Software is furnished to do so,
	subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all 
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
	INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
	PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
	OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
	SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# iplookup

This code is part of the project [High-Speed Router Functions](https://scikon.uni-konstanz.de/en/projects/1927/).
IPv6 packet forwarding still is a major bottleneck. Especially in the internet
core we face very large routing tables with millions of entries and a large
number of high-speed links. 

We conducted several experiments on algorithms and data structures to find and
design a data structure for high-speed lookup applications with millions
of entries. The code in this repository presents several implementations
in different stages of development.

For documentation, publications and reports see the university 
[project page](https://scikon.uni-konstanz.de/en/projects/1927/).

## Content

There are several different programs and implementations in this repository,
each in its own directory.

### cppeht

An implementation of a very early study of the _Efficient Hash Table_ in
c++. We did use it for preliminary analysis and results. Although it runs really
fast we didn't follow the c++ path because we ran into serious memory problems when
working with large tables with millions of entries. There are some other c and c++
implementations that tried to add new features and get rid of the problem but
were not completed. 

### jeht

This is a java implementation of our _Efficient Hash Table_. It is not the
one used for evaluation since it does not implement all the features. The reason
is that the java implementation consumes vast amount of resources in the presence
of millions of entries and thus lead to heap space errors even on machines
with really large memory. We mainly used it to explore the effect of inserts,
deletes, pruning etc on the different online / offline structures.

The ant build file builds an executable jar file. Tested with Java 5,6,7.
When executed without parameters it starts a shell. Type 'h' for help or start
with 'java -jar jeht-<version> -h'. The code is not well tested and might throw
exceptions under certain circumstances. There is for example sth. wrong with 
the Huffman encoder when working with pruned tables. I might fix that, if
I have the time and gusto for that.

### pyeth

This is a python implementation of the _Efficient Hash Table_. This is the 
actual implementation we used for evaluation. It might be the ugliest code
you'll ever see in your life. In addition it is extremely slow. However, it
is a complete simulator of the described EHT.

