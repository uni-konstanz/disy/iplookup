/*
Copyright (c) 2009 thomas.zink _at_ uni-konstanz _dot_ de

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package jeht.kn.uni.disy;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;
import java.util.TreeSet;

/**
 * 
 */

/**
 * @author zink
 *
 */
public final class FastHashTable<K,V> {

	// by Contructor
	private final int n;								// number of elements
	private final int m;								// bf len
	private final int w;								// bucket width
	private final int word;							// sram word size
	
	// by calculation
	
	
	// updated during runtime
	private int fpcount = 0;						// fp counter
	private int querycount = 0;						// query counter
	private int size = 0;							// num unique items
	private int offsize = 0;						// num items in off table
	private int onlsize = 0;						// num items in onl table
	
	// data structures
	private AbstractCountingBloomFilter<Short> cbf;
	private final AbstractHash<K> hash;
	private final ArrayList<Entry<K,V>>[] off;
	private ArrayList<Entry<K,V>>[] onl;
	private Hashtable<K,Entry<K,V>> cam;
	
	// other
	private boolean pruned = false;
	
	@SuppressWarnings("unchecked")
	public FastHashTable(final int n, final int m, int k, final int w, final int word, final int bits) {
		this.n = n;
		this.m = Calc.m_fht(this.n) >> m;
		this.w = w;
		this.word = word;
		k = k==0 ? Calc.k_bf(this.n, this.m) : k;
		cbf = new CountingBloomFilter(this.m);
		hash = new Uhash<K>(k, bits, this.m-1);
		off = new ArrayList[this.m];
		for (int i=0; i<this.m; i++) {
			off[i] = new ArrayList<Entry<K,V>>(1);
		}
	}
	
	public final HashCounter hash(final K key) {
		return new HashCounter(key);
	}
	
	public final boolean insert(final K key, V value) {
		final HashCounter hc = new HashCounter(key);
		Entry<K,V> e = new Entry<K,V>(key,value,hc.mli);
		final int index = off[hc.mli].indexOf(e);
		if (index!=-1) {
			e = off[hc.mli].get(index);
			e.setV(value);
			return false;
		}
		for (Integer i : hc.hashs) {
			off[i].add(e);
			offsize++;
			cbf.inc(i);
		}
		if (pruned) {
			relocateInsert(hc.hashs);
			insertOnline(e);
		}
		size++;
		return true;
	}
	
	public final V get(final K key) {
		this.querycount++;
		final HashCounter hc = new HashCounter(key);
		if (hc.minCounter == 0) return null;
		Entry<K,V> e = new Entry<K,V>(key);
		V r = null;
		if (!pruned) {
			Integer index = off[hc.mli].indexOf(e);
			if (index == -1) {
				this.fpcount++;
				return r;
			}
			r =  off[hc.mli].get(index).getV();
		} else {
			try {
				r = onl[hc.mli].get(onl[hc.mli].indexOf(e)).getV();
			} catch (ArrayIndexOutOfBoundsException ax) {
				try {
					r = cam.get(e.getK()).getV();
				} catch (NullPointerException nx) {
					this.fpcount++;
				}
			}
		}
		return r;
	}
		
	public final boolean remove(final K key) {
		boolean r;
		if (pruned) { r = removeP(key); }
		else { r = removeB(key); }
		if (r) { size--; }
		return r;
	}
	
	private final boolean removeB(final K key) {
		final HashCounter hc = new HashCounter(key);
		for (Integer i : hc.hashs) {
			if (!off[i].remove(new Entry<K,V>(key))) { return false; }
			offsize--;
			cbf.dec(i);
		}
		return true;
	}
			
	private final boolean removeP(final K key) {
		final HashCounter hc = new HashCounter(key);
		final Hashtable<K,HashCounter> relocation = new Hashtable<K,HashCounter>();
		for (Integer i : hc.hashs) {
			// remove from off table
			if (!off[i].remove(new Entry<K,V>(key))) { return false; }
			// add entires to relocation list
			for (Entry<K,V> e : off[i]) {
				K k = e.getK();
				if (relocation.get(k)==null) {
					relocation.put(k, new HashCounter(k));
				}
			}
			offsize--;
			// decrease counter
			cbf.dec(i);
		}
		// remove from onl/cam
		removeOnline(hc);
		// relocate
		relocateRemove(relocation);
		size--;
		return true;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public final void prune() {
		onl = new ArrayList[this.m];
		for (int i=0; i<this.m; i++) { onl[i] = new ArrayList<Entry<K,V>>(1); }
		cam = new Hashtable<K,Entry<K,V>>();
		for (int i=0; i<off.length; i++) {
			for (Entry<K,V> e : off[i]) {
				final HashCounter hc = new HashCounter(e.getK());
				if (i==hc.mli) {
					e.setH(i);
					insertOnline(e);
				}
			}
		}
		Hashtable<Short,Integer> dist = cbf.getDistribution();
		Short[] filter = cbf.getFilter();
		cbf = (AbstractCountingBloomFilter)new HuffmanCountingBloomFilter(dist, filter, (short)word);
		this.pruned = true;
	}
	
	private final void insertOnline(Entry<K,V> e) {
		if (onl[e.getH()].size()<this.w) {
			onl[e.getH()].add(e);
			onlsize++;
		}
		else { cam.put(e.getK(), e); }
	}
	
	private final void removeOnline(HashCounter hc) {
		if (onl[hc.mli].remove(new Entry<K,V>(hc.key))) { onlsize--; }
		else { cam.remove(hc.key); }
	}
	
	@SuppressWarnings("unchecked")
	private final void relocateInsert(ArrayList<Integer> hashs) {
		for (Integer h : hashs) {
			final ArrayList<Entry<K,V>> onle = (ArrayList<Entry<K,V>>)onl[h].clone();
			for (Entry<K,V> e : onle) {
				final HashCounter hc = new HashCounter(e.getK());
				if (hc.mli!=e.getH()) {
					onl[h].remove(e);
					onlsize--;
					e.setH(hc.mli);
					insertOnline(e);
				}
			}
		}
		final Object[] karr = new Object[cam.keySet().size()];
		cam.keySet().toArray(karr);
		for (K k : (K[])karr) {
			final Integer h = cam.get(k).getH();
			if (hashs.contains(h)) {
				final HashCounter hc = new HashCounter(k);
				if (hc.mli!=h) {
					final Entry<K,V> e = cam.get(k);
					e.setH(hc.mli);
					cam.remove(k);
					insertOnline(e);
				}
			}
		}
	}
	
	private final void relocateRemove(Hashtable<K,HashCounter> ht) {
		for (K key : ht.keySet()) {
			final HashCounter pre = ht.get(key);
			final HashCounter post = new HashCounter(key);
			if (pre.mli!=post.mli) {
				final Entry<K,V> e = new Entry<K,V>(key);
				Entry<K,V> reloc;
				try {
					final Integer index = onl[pre.mli].indexOf(e);
					reloc = onl[pre.mli].get(index);
					onl[pre.mli].remove(index);
					onlsize--;
				} catch (ArrayIndexOutOfBoundsException x) {
					reloc = cam.get(key);
					cam.remove(key);
				}
				reloc.setH(post.mli);
				insertOnline(reloc);
			}
		}
	}
	
	private final class HashCounter {
		private final K key;
		private final ArrayList<Integer> hashs;			// hash values
		private final ArrayList<Short> counters;		// retrieved counters
		private Short minCounter = Short.MAX_VALUE;				// min counter value
		private Integer mli;					// most left index
		
		public HashCounter(final K key) {
			this.key = key;
			hashs = new ArrayList<Integer>(new TreeSet<Integer>(hash.hash(this.key)));
			counters = cbf.get(hashs.toArray(new Integer[hashs.size()]));
			for (Short i : counters) minCounter = (short)Math.min(i, minCounter);
			mli = hashs.get(counters.indexOf(minCounter));
		}
		
		@Override
		public final String toString() {
			final StringBuilder sb = new StringBuilder();
			sb.append('(').append(key).append(',');
			sb.append(hashs).append(',');
			sb.append(counters).append(',');
			sb.append(minCounter).append(',');
			sb.append(mli).append(')');
			return sb.toString();
		}
	}
	
	@Override
	public final String toString() {
		final StringBuilder sb = new StringBuilder();
		// fht info
		sb
		.append(this.n).append(',')
		.append(this.m).append(',')
		.append(this.hash.getNum()).append(',')
		.append(this.w).append(',')
		.append(this.size).append(',')
		.append(cbf.getMax()).append(',')
		.append(cbf.getCounterwidth()).append(',')
		.append(cbf.getBits()).append(',')
		.append(this.offsize).append(',');
		if (pruned) {
			sb.append(this.onlsize).append(',')
			.append(this.cam.size()).append(',');
		} else {
			sb.append("0,0,");
		}
		return sb.toString();
	}
	
	public final Hashtable<Integer,Integer> distributionOff() {
		final Hashtable<Integer,Integer> offd = new Hashtable<Integer,Integer>();
		for (int m=0; m<this.m; m++) {
			final Integer key = off[m].size();
			Integer value = offd.get(key);
			if (value==null) offd.put(key, value);
			else value++;
		}
		return offd;
	}
	
	public final Hashtable<Integer,Integer> distributionOnl() {
		final Hashtable<Integer,Integer> onld = new Hashtable<Integer,Integer>();
		for (int m=0; m<this.m; m++) {
			final Integer key = onl[m].size();
			Integer value = onld.get(key);
			if (value==null) onld.put(key, value);
			else value++;
		}
		return onld;
	}
	
	public final int getN() { return n; }

	public final int getM() { return m; }

	public final int getW() { return w; }

	public final int getWord() { return word; }

	public final int getFpcount() { return fpcount; }

	public final int getQuerycount() { return querycount; }

	public final int getSize() { return size; }

	public final int getOffsize() { return offsize; }

	public final int getOnlsize() { return onlsize; }

	public final int getCamsize() { return cam.size(); }

	public final AbstractCountingBloomFilter<Short> getCbf() { return cbf; }

	public final AbstractHash<K> getHash() { return hash; }

	public final ArrayList<Entry<K, V>>[] getOff() { return off; }

	public final ArrayList<Entry<K, V>>[] getOnl() { return onl; }

	public final Hashtable<K, Entry<K, V>> getCam() { return cam; }

	public final boolean isPruned() { return pruned; }

	/**
	 * @param args
	 */
	public static final void main(final String[] args) {
		int N = 20, M = 5, K = 4, W = 3, WORD = 32, BITS = 32;
		FastHashTable<Integer,Integer> fht = new FastHashTable<Integer,Integer>(N,M,K,W,WORD,BITS);
		ArrayList<Entry<Integer,Integer>> items = new ArrayList<Entry<Integer,Integer>>(N);
		Random rnd = new Random();
		for (int i=0; i<N; i++) {
			Integer key = rnd.nextInt();
			Integer value = rnd.nextInt(Integer.valueOf(Short.MAX_VALUE));
			fht.insert(key, value);
			items.add(new Entry<Integer,Integer>(key,value));
		}
		System.out.print(fht.toString());
		int correct = 0;
		for (Entry<Integer,Integer> e : items) {
			int k = e.getK();
			Integer ret = fht.get(k);
			if (ret==e.getV()) correct++;
			else System.out.println("Could not retrieve " + e.getK());
		}
		System.out.println("Retrieved: " + correct + " of " + items.size() + " items.");
		correct = 0;
		for (int i=0; i<N/2; i++) {
			fht.remove(items.get(i).getK());
		}
		System.out.print(fht.toString());
		for (int i=0; i<N/2; i++) {
			fht.insert(items.get(i).getK(), items.get(i).getV());
		}
		fht.prune();
		System.out.print(fht.toString());
		for (Entry<Integer,Integer> e : items) {
			int k = e.getK();
			Integer ret = fht.get(k);
			if (ret==e.getV()) correct++;
			else System.out.println("Could not retrieve " + e.getK());
		}
		System.out.println("Retrieved: " + correct + " of " + items.size() + " items.");
		for (int i=0; i<N/2; i++) {
			fht.remove(items.get(i).getK());
			items.remove(i);
		}
		correct = 0;
		for (Entry<Integer,Integer> e : items) {
			int k = e.getK();
			Integer ret = fht.get(k);
			if (ret==e.getV()) correct++;
			else System.out.println("Could not retrieve " + e.getK());
		}
		System.out.println("Retrieved: " + correct + " of " + items.size() + " items.");
		for (int i=0; i<N/2; i++) {
			Integer key = rnd.nextInt();
			Integer value = rnd.nextInt(Integer.valueOf(Short.MAX_VALUE));
			fht.insert(key, value);
			items.add(new Entry<Integer,Integer>(key,value));
		}
		System.out.print(fht.toString());
		correct = 0;
		for (Entry<Integer,Integer> e : items) {
			int k = e.getK();
			Integer ret = fht.get(k);
			if (ret==e.getV()) correct++;
			else System.out.println("Could not retrieve " + e.getK());
		}
		System.out.println("Retrieved: " + correct + " of " + items.size() + " items.");
	}

}
