/*
Copyright (c) 2009 thomas.zink _at_ uni-konstanz _dot_ de

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package jeht.kn.uni.disy;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Random;

/**
 * 
 */

/**
 * @author zink
 *
 */
public class Main {
	private int N = 10000;
	private int M = 0;
	private int K = 0;
	private int W = 4;
	private int BITS = 32;
	private int WORD = 32;
	private int RUNS = 1;
	//private int NTHREADS = 4;
	private long TIME;
	private boolean CLI = false;
	private static String PROMPT = "jEHT> ";
	private static String WELCOME =
		"----------------------------------------\n" +
		"|    Huawei - Konstanz - IP Lookup     |\n" +
		"|        Efficient Hash Tables         |\n" +
		"----------------------------------------\n" +
		"\n" +
		"Welcome. Start by setting the configuration.\n" +
		"Then create the table with command eht." +
		"Then you can insert and query values, prune and explore.\n"+
		"Type 'h' for help.";
	
	private FastHashTable<Integer,Short> fht;
	private Hashtable<Integer,Short> table;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Main m = new Main();
		m.args(args);
		if (m.CLI) m.cli();
		else m.batch();
	}
	
	private void batch() {
		for (int r=0; r<RUNS; r++) {
			TIME = System.nanoTime();
			createFht();
			insert();
			fht.prune();
			System.out.println(fht.toString());
			get();
			System.gc();
			TIME = mem(TIME);
		}
	}
	
	

	private void cli() {
		System.out.println(WELCOME);
		while(CLI) {
			String[] cmdline = getCommandline();
			switchCommand(cmdline);
		}
		System.out.println("Thank you, bye bye");
	}

	private String[] getCommandline() {
		System.out.print(PROMPT);
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String input = "";
		try {
			input = in.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return input.split("\\s");
	}
	
	private void switchCommand(String[] cmdline) {
		if (cmdline[0].equals("")) return;
		try {
			if (cmdline[0].equals("h")) help();
			else if (cmdline[0].equals("q")) CLI=false;
			else if (cmdline[0].equals("n")) N = Integer.valueOf(cmdline[1]);
			else if (cmdline[0].equals("m")) M = Integer.valueOf(cmdline[1]);
			else if (cmdline[0].equals("k")) K = Integer.valueOf(cmdline[1]);
			else if (cmdline[0].equals("w")) W = Integer.valueOf(cmdline[1]);
			else if (cmdline[0].equals("bits")) BITS = Integer.valueOf(cmdline[1]);
			else if (cmdline[0].equals("word")) WORD = Integer.valueOf(cmdline[1]);
			else if (cmdline[0].equals("cfg")) 
				System.out.println("n:" + N + " m:" + M + " k:" + K + " w:" + W + " bits:" + BITS + " word:" + WORD);
			else if (cmdline[0].equals("eht")) createFht();
			else if (cmdline[0].equals("prune")) {
				if (fht==null) System.out.println("you need to create an eht first.\n");
				else fht.prune();
			}
			else if (cmdline[0].equals("print")) {
				if (fht==null) {
					System.out.println("you need to create an eht first.\n");
					return;
				}
				if (cmdline.length==1) System.out.println(fht.toString());
				else {
					if (cmdline[1].equals("cbf")) {
						if (cmdline.length>2) System.out.println(fht.getCbf().get(Integer.valueOf(cmdline[2])).toString());
						else System.out.println(fht.getCbf().toString());
					}
					if (cmdline[1].equals("off")) {
						if (cmdline.length>2) System.out.println(fht.getOff()[Integer.valueOf(cmdline[2])].toString());
						else {
							for (int i=0; i<fht.getOff().length; i++) {
								System.out.print("["+i+"] : "+fht.getOff()[i].toString()+'\n');
							}
						}
					}
					if (cmdline[1].equals("onl")) {
						if (cmdline.length>2) System.out.println(fht.getOnl()[Integer.valueOf(cmdline[2])].toString());
						else {
							for (int i=0; i<fht.getOnl().length; i++) {
								System.out.print("["+i+"] : "+fht.getOnl()[i].toString()+'\n');
							}
						}
					}
					if (cmdline[1].equals("cam")) System.out.println(fht.getCam().toString());
				}
			}
			else if (cmdline[0].equals("hash")) System.out.println(fht.hash(Integer.valueOf(cmdline[1])));
			else if (cmdline[0].equals("insert")) System.out.println(fht.insert(Integer.valueOf(cmdline[1]), Short.valueOf(cmdline[2])));
			else if (cmdline[0].equals("get")) System.out.println(fht.get(Integer.valueOf(cmdline[1])));
			else if (cmdline[0].equals("remove")) System.out.println(fht.remove(Integer.valueOf(cmdline[1])));
			else if (cmdline[0].equals("fill")) insert();
			else if (cmdline[0].equals("getall")) get();
			else System.out.println("command unknown, try 'h' for help.");
		} catch (Exception x) {
			x.printStackTrace();
		}
	}
	
	private void createFht() {
		fht = new FastHashTable<Integer,Short>(N,M,K,W,WORD,BITS);
		table = new Hashtable<Integer,Short>(N);
	}
	
	private void insert() {
		Random rnd = new Random();
		for (int n=0; n<N; n++) {
			Integer key = rnd.nextInt();
			Short value = (short)rnd.nextInt(Short.MAX_VALUE);
			fht.insert(key, value);
			table.put(key, value);
		}
	}
	
	private void get() {
		for (Integer key : table.keySet()) {
			Short value = fht.get(key);
			if (value!=table.get(key)) {
				System.err.println("Error retrieving: " + key);
			}
		}
	}
		
	private void args(String[] args) {
		if (args.length == 0) {
			CLI = true;
			return;
		}
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-n")) N = Integer.valueOf(args[i + 1]);
			if (args[i].equals("-m")) M = Integer.valueOf(args[i + 1]);
			if (args[i].equals("-k")) K = Integer.valueOf(args[i + 1]);
			if (args[i].equals("-w")) W = Integer.valueOf(args[i + 1]);
			if (args[i].equals("-b")) BITS = Integer.valueOf(args[i + 1]);
			if (args[i].equals("-c")) WORD = Integer.valueOf(args[i + 1]);
			if (args[i].equals("-s")) RUNS = Integer.valueOf(args[i + 1]);
			if (args[i].equals("-h")) { usage(); System.exit(0); }
		}
	}
	
	private final long mem(long TIME) {
		// display timing information
		System.err.println("# Time: " + (System.nanoTime() - TIME) / 1000 + " mus");
		// memory stuff
		Runtime r = Runtime.getRuntime();
		for (int i =0; i < 4; i++) { r.gc(); }
		long a = (r.totalMemory() - r.freeMemory());
		boolean b = a > 1 << 20;
		long f = b ? 20 : 10;
		System.err.println("# Mem:  " + (a + (1 << f - 1) >> f) + (b ? " mb" : " kb"));
		// set new timer
		return System.nanoTime();
	}
	
	private void usage() {
		System.out.println(
			"Usage: java -jar jfht.jar [options]\n"
			+ "\n"
			+ "\t-n [num]: number of items to insert\n"
			+ "\t-m [num]: number of right shifts of optimal bloom filter length\n"
			+ "\t-k [num]: number of hash functions to use, none or 0 for optimal\n"
			+ "\t-r [num]: number of bits to check for hash calculation\n"
			+ "\t-c [num]: word size of compression\n"
			+ "\t-s [num]: number of simulation runs\n"
			+ "\t-t [num]: number of threads\n"
			+ "\n"
		);
	}

	private void help() {
		System.out.println(
			"Available Commands\n\n"
			+ "n [num]: set number of items to insert\n"
			+ "m [num]: set number of right shifts of optimal bloom filter length\n"
			+ "k [num]: set number of hash functions to use, none or 0 for optimal\n"
			+ "bits [num]: set number of bits to check for hash calculation\n"
			+ "word [num]: word size of compression\n"
			+ "cfg: prints current settings\n"
			+ "eht: creates the new EfficientHashTable\n"
			+ "prune: prunes the table and adds online data structures\n"
			+ "insert [key] [val]: inserts the key/value pair\n"
			+ "get [key]: queries the table for key\n"
			+ "remove [key]: removes the key from the table\n"
			+ "fill: fills the table with n randomly generated keys and values\n"
			+ "print [str]: prints the data structures\n"
			+ "\tall: whole fht\n"
			+ "\tcbf (num): counting bloom filter, or cell (num)\n"
			+ "\toff (num): offline table, or bucket (num)\n"
			+ "\tonl (num): online table, or bucket (num)\n"
			+ "\tcam: prints the cam\n"
			+ "h: prints this help\n"
			+ "q: quit\n"
		);
	}
}
