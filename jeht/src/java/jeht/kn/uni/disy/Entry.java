/*
Copyright (c) 2009 thomas.zink _at_ uni-konstanz _dot_ de

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package jeht.kn.uni.disy;
/**
 * 
 */

/**
 * @author zink
 *
 */
public final class Entry<K,V> {
	private final K k;
	private V v;
	private Integer h;
	
	public Entry(final K key, final V value, final Integer hash) {
		this.k = key;
		this.v = value;
		this.h = hash;
	}
	
	public Entry(final K key, final V value) {
		k = key;
		v = value;
		//h = null;
	}
	
	public Entry(final K key) {
		k = key;
		//v = null;
		//h = null;
	}
	
	@Override
	public final String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(this.k).append('=').append('(').append(this.v).append(',').append(this.h).append(')');
		return sb.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((k == null) ? 0 : k.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		Entry<K,V> other = (Entry<K,V>) obj;
		if (k == null) {
			if (other.k != null)
				return false;
		} else if (!k.equals(other.k))
			return false;
		return true;
	}
	
	public final V getV() { return v; }

	public final void setV(final V v) { this.v = v; }

	public final Integer getH() { return h; }

	public final void setH(final Integer h) { this.h = h; }

	public final K getK() { return k; }
}
