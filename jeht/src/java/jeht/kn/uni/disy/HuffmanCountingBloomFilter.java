/*
Copyright (c) 2009 thomas.zink _at_ uni-konstanz _dot_ de

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package jeht.kn.uni.disy;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * @author zink
 *
 */
public final class HuffmanCountingBloomFilter extends
		AbstractCountingBloomFilter<String> {

	private Huffman<Short> huff;
	private final Short maxlen;
	private Short rate;
	
	public HuffmanCountingBloomFilter(final Hashtable<Short,Integer> distribution, final Short[] cbf, final Short maxlen) {
		this.huff = new Huffman<Short>(distribution);
		this.rate = maxlen;
		this.maxlen = maxlen;
		this.filter = encodeFilter(cbf);
	}
	
	private final String[] encodeFilter(final Short[] cbf) {
		ArrayList<String> ccbf = new ArrayList<String>();
		// find best compression rate by iteratively filling whole word
		String word = new String();
		int ncells = 0; 
		int i = 0;
		// sth wrong here when working with pruned tables.
		while (i<cbf.length) {
			String code = huff.getCode(cbf[i++]);
			if (ncells<rate) {
				word += code;
				ncells++;
				if (word.length()>maxlen) {
					ccbf.clear();
					word = "";
					ncells = 0;
					rate--;
					i = 0;
				}
			} else {
				// whats with the last counters / word?
				ccbf.add(word);
				word = code;
				ncells = 1;
			}
		}
		return ccbf.toArray(new String[ccbf.size()]);
	}
	
	@Override
	public void dec(Integer... a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ArrayList<Short> get(Integer... a) {
		ArrayList<Short> r = new ArrayList<Short>();
		for (Integer i : a) {
			Object[] plain = huff.decode(filter[i/rate]);
			int index = i-(i/rate)*rate;
			r.add((Short)plain[index]);
		}
		return r;
	}

	@Override
	public int getBits() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getCounterwidth() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Short getMax() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void inc(Integer... a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void set(String t, Integer... a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Hashtable<Short, Integer> getDistribution() {
		// TODO Auto-generated method stub
		return null;
	}
}
