/*
Copyright (c) 2009 thomas.zink _at_ uni-konstanz _dot_ de

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in 
the Software without restriction, including without limitation the rights to use, 
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package jeht.kn.uni.disy;
/**
 * 
 */

/**
 * @author zink
 *
 */
public abstract class AbstractBloomFilter<T> {
	T[] filter;
		
	// Interface
	public abstract void set(T t, final Integer ... a);
	public abstract void clear();
	
	//abstract public String asString();
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("|");
		for (T i: filter) {
			sb.append(i.toString()).append("|");
		} 
		return sb.toString();
	}
	// Getter / Setter
	public final T[] getFilter() { return filter; }
	
	public final T getFilter(final int i) { return filter[i]; }
	
	public final void setFilter(final int i, final T v) { filter[i] = v; }

	public final int length() { return filter.length; }
	
}
